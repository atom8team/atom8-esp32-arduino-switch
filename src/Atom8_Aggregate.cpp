#include "Atom8_Constants.h"
#include "Atom8_Aggregate.h"
#include "Atom8_MCU_Switch.h"
#include "Atom8_MCU_Sensor.h"
#include "Atom8_Wifi.h"
#include "Atom8_Scene.h"
#include "Atom8_Utils.h"
#include "Atom8_Logging.h"

String Atom8Aggregate::getInfo(String facet) {
  if(atom8Utils.isEmpty(facet)) facet = "high";
  DynamicJsonBuffer jsonBuffer;

  JsonObject& responseJson = jsonBuffer.createObject();
  if(facet =="high") {
    JsonArray& instanceList = jsonBuffer.createArray();
    #ifdef ATOM8_SWITCH_MODULE
    for(int i=0; i < 4; i++) {
      JsonObject& instanceDetail = jsonBuffer.createObject();
      instanceDetail["value"] = atom8MCUSwitch.getSwitch(i);
      instanceDetail["id"] = i;
      instanceDetail["genericDeviceType"] = GENERIC_DEVICE_TYPE_BINARY_SWITCH;
      instanceList.add(instanceDetail);
    }
    #endif

    #ifdef ATOM8_SENSOR_MODULE
    JsonObject& instanceDetail = jsonBuffer.createObject();
    instanceDetail["id"] = 1;
    instanceDetail["genericDeviceType"] = BINARY_SENSOR;
    instanceList.add(instanceDetail);
    #endif


    JsonArray& sceneListJson = jsonBuffer.createArray();
    std::list<String> allScenes = atom8Scene.getAll();
    for (std::list<String>::iterator it=allScenes.begin(); it != allScenes.end(); ++it) {
      String sceneJsonStr = *it;
      JsonObject& sceneBO = jsonBuffer.parseObject(sceneJsonStr);
      sceneListJson.add(sceneBO);
    }
    responseJson["instanceList"] = instanceList;
    responseJson["sceneList"] = sceneListJson;
  }

  responseJson["address"] = atom8Utils.ipAddressToString(atom8WifiManager.getIp());
  responseJson["signal"] = atom8WifiManager.rssi();
  responseJson["status"] = HTTP_SUCCESS;

  String nodeId = atom8WifiManager.getSoftAPSSID();
  responseJson["id"] = nodeId;
  responseJson["ssid"] = atom8WifiManager.getSSID();
  responseJson["bssid"] = atom8WifiManager.getBSSID();

  #ifdef ATOM8_SWITCH_MODULE
  responseJson["versionCode"] = ATOM8_FIRMWARE_SWITCH_VERSION_CODE;
  responseJson["versionName"] = ATOM8_FIRMWARE_SWITCH_VERSION_NAME;
  #endif

  #ifdef ATOM8_SENSOR_MODULE
  responseJson["versionCode"] = ATOM8_FIRMWARE_SENSOR_VERSION_CODE;
  responseJson["versionName"] = ATOM8_FIRMWARE_SENSOR_VERSION_NAME;
  #endif

  String responseJsonStr;
  responseJson.printTo(responseJsonStr);
  return responseJsonStr;
}

Atom8Aggregate atom8Aggregate;
