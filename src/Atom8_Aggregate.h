// Atom8_Console.h

#ifndef _ATOM8_AGGREGATE_h
#define _ATOM8_AGGREGATE_h

#include "ArduinoJson.h"


class Atom8Aggregate {
 public:
	 String getInfo(String);
};

extern Atom8Aggregate atom8Aggregate;

#endif
