
#ifndef _ATOM8_AUTOMATION_EVENTS_h
#define _ATOM8_AUTOMATION_EVENTS_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
	#include "ArduinoJson.h"
	#include <PubSubClient.h>
#else
	#include "WProgram.h"
#endif

class Atom8AutomationEvents
{
 public:
	 void sensorMotionDetected(String sensorEventJson);
	 void locationDetected(String locationEventJson);
   void timeDetected(String timeEventJson);
};

extern Atom8AutomationEvents Atom8AutomationEvents;

#endif
