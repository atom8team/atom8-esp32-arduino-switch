#include "Arduino.h"

class Atom8MCUSensor {
private:
	void handleMotionNotDetectedAction();
public:
	void setup();
	void loop();
};

extern Atom8MCUSensor atom8MCUSensor;
