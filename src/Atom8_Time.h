#include "ArduinoJson.h"

class Atom8Time {
private:
  long currentTime = 0;
  long millisSinceMidnight = 0;
public:
  void setup();
  long getCurrentTime();
  void setCurrentTime(long currentTime);
  long getMillisSinceMidnight();
  void setMillisSinceMidnight(long millisSinceMidnight);
};

extern Atom8Time atom8Time;
