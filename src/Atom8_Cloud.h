// Atom8_Cloud.h

#ifndef _ATOM8_CLOUD_h
#define _ATOM8_CLOUD_h
#include "ArduinoJson.h"

class Atom8_Cloud
{

private:
	void publishPrimingRequest();
public:
  void setup();
  void loop();
  void publish(String publishStr);
  bool isReady();
};

extern Atom8_Cloud atom8Cloud;

#endif
