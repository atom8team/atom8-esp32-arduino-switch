#include "Atom8_Events_Incoming.h"
#include "ArduinoJson.h"
#include "Atom8_MCU_Switch.h"
#include "Atom8_Events_Outgoing.h"
#include "Atom8_OTA.h"
#include "Atom8_Time.h"


void Atom8EventsIncoming::handlePutSwitchEvent(String callbackJsonStr) {
	DynamicJsonBuffer jsonBuffer;
	JsonObject& requestJson = jsonBuffer.parseObject(callbackJsonStr);
	double value = requestJson["value"];
	String instance = requestJson["instance"].asString();

	// Process
	if (instance) {
		atom8MCUSwitch.putSwitch(instance.toInt(), value);
		atom8EventsOutgoing.putInstanceEvent(instance.toInt(), value, "cloud_network");
	}
}

void Atom8EventsIncoming::handleOTAUpdateEvent(String callbackResponse) {
  DynamicJsonBuffer jsonBuffer;
  JsonObject& jsonObject = jsonBuffer.parseObject(callbackResponse);
  String bucket = jsonObject["bucket"];
  String imagePath = jsonObject["imagePath"];
  atom8OTA.update(bucket, imagePath);
}

void Atom8EventsIncoming::handleTimeUpdateEvent(String callbackJsonStr) {
  DynamicJsonBuffer jsonBuffer;
  JsonObject& responseJson = jsonBuffer.parseObject(callbackJsonStr);
  atom8Time.setCurrentTime(responseJson["timestamp"]);
  atom8Time.setMillisSinceMidnight(responseJson["millisSinceMidnight"]);
}

Atom8EventsIncoming atom8EventsIncoming;
