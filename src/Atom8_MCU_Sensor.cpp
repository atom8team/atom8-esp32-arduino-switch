#include "Atom8_MCU_Sensor.h"
#include "Atom8_MDNS.h"

#include "ArduinoJson.h"
#include "Atom8_Constants.h"
#include "Atom8_Scene.h"
#include "Esp32Ticker.h"

long MOTION_SENSOR_DELAY = ONE_SEC_MILLIS * 15;

int MOTION_INTERRUPT_PIN = 10;

int MOTION_DETECTED_INDICATOR_LED = 2;

long lastMotionDetected = millis();

Ticker motionDetectedTicker;

void turnOffMotionDetectedIndicator() {
	digitalWrite(MOTION_DETECTED_INDICATOR_LED, HIGH);
}

void showMotionDetectedIndicator() {
	digitalWrite(MOTION_DETECTED_INDICATOR_LED, LOW);
}

void motion() {
	if(digitalRead(MOTION_INTERRUPT_PIN) == 1) {
		atom8Scene.setMotionDetected(millis());
		showMotionDetectedIndicator();
	} else {
		turnOffMotionDetectedIndicator();
	}
}

void Atom8MCUSensor::setup() {
	pinMode(MOTION_DETECTED_INDICATOR_LED, OUTPUT);
  pinMode(MOTION_INTERRUPT_PIN, INPUT_PULLUP);
  attachInterrupt(MOTION_INTERRUPT_PIN, motion, CHANGE);
}

void Atom8MCUSensor::loop() {
}

Atom8MCUSensor atom8MCUSensor;
