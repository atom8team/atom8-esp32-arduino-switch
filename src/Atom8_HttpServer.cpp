#include "Atom8_Constants.h"
#include "Atom8_HttpServer.h"
#include "AsyncTCP.h"
#include "ESPAsyncWebServer.h"
#include "ArduinoJson.h"
#include "Atom8_Wifi.h"
#include "Atom8_MCU_Switch.h"
#include "Atom8_Utils.h"
#include "Atom8_Storage.h"
#include "Atom8_Scene.h"
#include "Atom8_Aggregate.h"
#include "Atom8_Events_Outgoing.h"
#include <DNSServer.h>

AsyncWebServer server(80);


const byte DNS_PORT = 53;
IPAddress apIP(192, 168, 1, 1);
DNSServer dnsServer;

/**
 * Private Members
 */
 void handleDummyOnRequest(AsyncWebServerRequest *request) {
	 // Dummy request handlers for PUT and I dnt know if we can pass null instead of this
 }

void handleRoot(AsyncWebServerRequest *request) {
	request->send(200, "text/plain", "hello from esp8266!");
}

void handleNotFound(AsyncWebServerRequest *request) {
	request->send(404, "text/plain", "Not Found");
}

void sendJsonResponse(AsyncWebServerRequest *request, JsonObject& responseJson) {
	AsyncResponseStream *response = request->beginResponseStream("application/json");
	responseJson.printTo(*response);
	request->send(response);
}

void handleAtom8Discovery(AsyncWebServerRequest *request) {
  // Response
  DynamicJsonBuffer jsonBuffer;
  JsonObject& responseJson = jsonBuffer.parseObject(atom8Aggregate.getInfo("high"));
  sendJsonResponse(request, responseJson);
}

void handlePutInstance(AsyncWebServerRequest *request, uint8_t *data, size_t len, size_t index, size_t total) {
	// Request
	DynamicJsonBuffer jsonBuffer;
	JsonObject& requestJson = jsonBuffer.parseObject(String((char *)data));
	double value = requestJson["value"];
	String instance = requestJson["instance"].asString();

	// Process
	if (instance) {
		atom8MCUSwitch.putSwitch(instance.toInt(), value);
    atom8EventsOutgoing.putInstanceEvent(instance.toInt(), value, "local_network");
	}

	//Response
	JsonObject& responseJson = jsonBuffer.createObject();
	responseJson["instance"] = instance;
	responseJson["value"] = atom8MCUSwitch.getSwitch(instance.toInt());
	sendJsonResponse(request, responseJson);
}

void handleGetStation(AsyncWebServerRequest* request) {
	// Response
	DynamicJsonBuffer jsonBuffer;
	JsonObject& responseJson = jsonBuffer.createObject();
	if(atom8WifiManager.isConnected()) {
    responseJson["ip"] = atom8Utils.ipAddressToString(atom8WifiManager.getIp());
	}
	responseJson["status"] = atom8WifiManager.getStatus();
	responseJson["enqueueStatus"] = atom8WifiManager.getEnqueueStatus();
	sendJsonResponse(request, responseJson);
}

void handlePutStation(AsyncWebServerRequest* request, uint8_t *data, size_t len, size_t index, size_t total) {

	// Request
	DynamicJsonBuffer jsonBufferRequest;
	JsonObject& requestJson = jsonBufferRequest.parseObject(String((char *)data));
	String ssid = requestJson["ssid"];
	String password = requestJson["password"];

	// Process
	Serial.print("SSID ");
	Serial.println(ssid);
	Serial.print("Password ");
	Serial.println(password);
	if (ssid.length() > 0 && password.length() > 0) {
		bool isConnected = atom8WifiManager.enqueueForConnectToAP(ssid, password);
	}

	// Response
	DynamicJsonBuffer jsonBufferResponse;
	JsonObject& responseJson = jsonBufferResponse.createObject();
	responseJson["status"] = HTTP_SUCCESS;
	sendJsonResponse(request, responseJson);
}

void handlePutScene(AsyncWebServerRequest* request, uint8_t *data, size_t len, size_t index, size_t total) {
  // Request
	DynamicJsonBuffer jsonBufferRequest;
	JsonObject& requestJson = jsonBufferRequest.parseObject(String((char *)data));
	// Process
  int status = atom8Scene.add(requestJson);
	// Response
  DynamicJsonBuffer jsonBufferResponse;
	JsonObject& responseJson = jsonBufferResponse.createObject();
  responseJson["status"] = status;
  sendJsonResponse(request, responseJson);
}

void handleDeleteScene(AsyncWebServerRequest* request) {
  DynamicJsonBuffer jsonBufferResponse;
  JsonObject& responseJson = jsonBufferResponse.createObject();
  if(request->hasArg("sceneId")) {
    String sceneId = request->arg("sceneId");
    atom8Scene.remove(sceneId);
    responseJson["status"] = HTTP_SUCCESS;
  } else {
    responseJson["status"] = HTTP_BAD_INPUT;
  }
  sendJsonResponse(request, responseJson);
}



void Atom8HttpServer::setupRoutes() {
	server.on("/", HTTP_GET, handleRoot);
	server.on("/atom8", HTTP_GET, handleAtom8Discovery);
	server.on("/station", HTTP_GET, handleGetStation);
	server.on("/station", HTTP_PUT, handleDummyOnRequest, NULL, handlePutStation);
	server.on("/instance", HTTP_PUT, handleDummyOnRequest, NULL, handlePutInstance);
  server.on("/scene", HTTP_PUT, handleDummyOnRequest, NULL, handlePutScene);
  server.on("/scene", HTTP_DELETE, handleDeleteScene);
}

/**
 * Public Members
 */
void Atom8HttpServer::setup() {
	Atom8HttpServer::setupRoutes();
	server.onNotFound(handleNotFound);
	server.begin();
	Serial.println("HTTP server started");
  // if DNSServer is started with "*" for domain name, it will reply with
  // provided IP to all DNS request
  // dnsServer.start(DNS_PORT, "*", apIP);

}

void Atom8HttpServer::loop() {
  // dnsServer.processNextRequest();
}

Atom8HttpServer atom8HttpServer;
