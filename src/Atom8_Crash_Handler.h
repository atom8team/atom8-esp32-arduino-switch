#include "Arduino.h"

class Atom8CrashHandler {
private:
public:
	bool hasCrash();
	void loop();
	String getCrashStr();
	void clear();
};

extern Atom8CrashHandler atom8CrashHandler;
