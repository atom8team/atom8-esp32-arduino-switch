#include "ArduinoJson.h"
#include "list"
#include "Atom8_KeyValue.h"

class Atom8Scene {
private:
  Atom8KeyValue sceneStorage = Atom8KeyValue("scenes");
  void storeSceneInFS(String sceneJsonStr);
  void handleDeviceAction(String nodeId, String instanceId, double value);
  void handleMotionActionForScenes(double value);
  void handleMotionDetectedForScenes();
  void handleMotionNotDetectedForScenes();
  void flushInMemoryStringToStorage();
public:
  void setup();
  void loop();
  int add(JsonObject& scene);
  bool remove(String sceneName);
  bool exists(String sceneName);
  void setMotionDetected(long motionDetectedTime);
  std::list<String> getAll();
  void evaluateAndExecuteScenesForSwitches();
};

extern Atom8Scene atom8Scene;
