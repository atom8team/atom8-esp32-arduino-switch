#include "Arduino.h"

class Atom8EventsIncoming {
private:
public:
	void setup();
	void loop();
  void handlePutSwitchEvent(String callbackJsonStr);
  void handleOTAUpdateEvent(String callbackJsonStr);
  void handleTimeUpdateEvent(String callbackJsonStr);
};

extern Atom8EventsIncoming atom8EventsIncoming;
