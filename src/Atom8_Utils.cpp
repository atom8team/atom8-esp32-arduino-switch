#include "Atom8_Utils.h"
#include "WiFi.h"
#include "ArduinoJson.h"


String Atom8Utils::ipAddressToString(IPAddress ip) {
	return String(ip[0]) + "." + String(ip[1]) + "." + String(ip[2]) + "." + String(ip[3]);
}

bool Atom8Utils::isEmpty(const char* input) {
	return (input == NULL) || (input[0] == '\0');
}

bool Atom8Utils::isNotEmpty(String input) {
	return !isEmpty(input);
}

bool Atom8Utils::isEmpty(String input) {
	if (!input) return true;
	input.trim();
	return input.length() <= 0;
}

String Atom8Utils::toString(uint64_t number) {
  unsigned long long1 = (unsigned long)((number & 0xFFFF0000) >> 16 );
  unsigned long long2 = (unsigned long)((number & 0x0000FFFF));

  String hex = String(long1, HEX) + String(long2, HEX); // six octets
	return hex;
}

String Atom8Utils::charAndLenToString(char* input, int inputLen) {
	char inputNew[inputLen+1];
	strncpy(inputNew,input,inputLen);
	inputNew[inputLen] = '\0';
	return String(inputNew);
}

Atom8Utils atom8Utils;
