#ifndef _ATOM8_OTA_h
#define _ATOM8_OTA_h
#endif
#include "ArduinoJson.h"


class Atom8_OTA {
private:
  void checkAndUpdate();
public:
  void setup();
  void loop();
	void update(String bucket, String imagePath);
};

extern Atom8_OTA atom8OTA;
