#include "AsyncTCP.h"
#include "ESPAsyncWebServer.h"

class Atom8HttpServer {
public:
	void setup();
	void loop();
private:
	void setupRoutes();
};

extern Atom8HttpServer atom8HttpServer;
