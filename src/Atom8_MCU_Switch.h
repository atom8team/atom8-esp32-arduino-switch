#include "Arduino.h"
#include "ArduinoJson.h"

class Atom8MCUSwitch {
private:

public:

	// SENSOR_VP, SENSOR_VN, 34, 35
	const byte INPUT_SWITCH_PIN[4] = { 36, 39, 34, 35 };

	void setup();
	void loop();
	double getSwitch(byte pin);
	void putSwitch(byte pin, double val);
	double getAnalogValue();
	void updateInfoToCloud();
};

extern Atom8MCUSwitch atom8MCUSwitch;
