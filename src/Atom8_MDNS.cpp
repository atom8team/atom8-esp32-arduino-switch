#include "Atom8_MDNS.h"
#include "ESPmDNS.h"
#include "Atom8_Wifi.h"
#include "map"
#include "Atom8_Utils.h"
#include "Atom8_Constants.h"
#include "Atom8_Logging.h"

boolean started = false;

boolean anyIpAddressEmpty = true;

std::map<String, String> ipAddressMap;



void Atom8_MDNS::refreshIpAddresses() {
  // Sensor to find the IP of the nearest switch and update it
  int n = MDNS.queryService("atom8", "tcp"); // Send out query for esp tcp services
  boolean anyIpAddressEmptyLocal = false;
  if (n == 0) {
    atom8Logging.info("no services found");
  } else {
    for (int i = 0; i < n; ++i) {
      String hostname = MDNS.hostname(i);
      IPAddress ip = MDNS.IP(i);
      String ipStr = String(ip[0]) + '.' + String(ip[1]) + '.' + String(ip[2]) + '.' + String(ip[3]);
      if(atom8Utils.isEmpty(ipStr)) boolean anyIpAddressEmptyLocal = true;
      ipAddressMap[hostname] = ipStr;
    }
  }
  anyIpAddressEmpty = anyIpAddressEmptyLocal;
}

long lastRefreshTime = 0;

void startMDNS() {
  if(!MDNS.begin(atom8WifiManager.getSoftAPSSID())) {
    atom8Logging.info("Error setting up MDNS responder!");
    return;
  } else {
    atom8Logging.info("mDNS responder started");
    MDNS.addService("_atom8", "_tcp", 80);
    started = true;
  }
}

void Atom8_MDNS::loop() {
  if(!started) {  
    startMDNS();
  }
}

String Atom8_MDNS::getIp(String deviceName) {
  deviceName.toLowerCase();
  return ipAddressMap[deviceName];
}

Atom8_MDNS atom8MDNS;
