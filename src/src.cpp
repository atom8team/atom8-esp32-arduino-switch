#include <Arduino.h>
#include "Atom8_Wifi.h"
#include <Arduino.h>
#include "Atom8_Constants.h"
#include "Esp32Ticker.h"
#include "Atom8_Wifi.h"
#include "Atom8_HttpServer.h"
#include "Atom8_Cloud.h"
#include "Atom8_OTA.h"
#ifdef ATOM8_SENSOR_MODULE
  #include "Atom8_MCU_Sensor.h"
#endif
#ifdef ATOM8_SWITCH_MODULE
  #include "Atom8_MCU_Switch.h"
#endif
#include "Atom8_MDNS.h"
#include "Atom8_Scene.h"
#include "Atom8_Events_Outgoing.h"
#include "Atom8_Crash_Handler.h"
#include "Atom8_Events_Incoming.h"
#include "Preferences.h"
#include "Atom8_Logging.h"
#include "Atom8_Storage.h"


#ifndef UNIT_TEST  // IMPORTANT LINE!

void loggingSetup(void);
void switchSetup(void);
void wifiSetup(void);
void cloudSetup(void);
void restSetup(void);
void switchLoop(void);
void wifiLoop(void);
void cloudLoop(void);
void restLoop(void);

void setup() {
  // put your setup code here, to run once:
  loggingSetup();
  switchSetup();
  wifiSetup();
  cloudSetup();
  restSetup();
}

void loop() {
  switchLoop();
  delay(0);
  wifiLoop();
  delay(0);
  cloudLoop();
  delay(0);
  restLoop();
  delay(0);
}

void loggingSetup() {
  atom8Logging.setup();
}

void switchSetup() {
  #ifdef ATOM8_SENSOR_MODULE
   atom8MCUSensor.setup();
  #endif

  #ifdef ATOM8_SWITCH_MODULE
  atom8MCUSwitch.setup();
  #endif
}

void wifiSetup() {
  atom8WifiManager.setup();
  atom8HttpServer.setup();
}

void cloudSetup() {
  atom8OTA.setup();
  atom8Cloud.setup();
}

void restSetup() {
  atom8Scene.setup();
}

void switchLoop() {
  // Physical switches should work irrespective of crash or not
  #ifdef ATOM8_SENSOR_MODULE
  atom8MCUSensor.loop();
  #endif
  #ifdef ATOM8_SWITCH_MODULE
  atom8MCUSwitch.loop();
  #endif
}

void wifiLoop() {
  atom8WifiManager.loop();
  delay(0);
  atom8HttpServer.loop();
  delay(0);
  if(!atom8WifiManager.isConnected()) return;
  atom8MDNS.loop();
  delay(0);
}

void cloudLoop() {
  if(!atom8WifiManager.isConnected())  {
    atom8Logging.info("disconnected");
    return;
  }
  // atom8OTA.loop();
  // delay(0);
  atom8Cloud.loop();
}

void restLoop() {
  if(!atom8WifiManager.isConnected()) return;
  delay(0);
  atom8EventsOutgoing.loop();
  delay(0);
  atom8Scene.loop();
  delay(0);
  atom8CrashHandler.loop();
  delay(0);
}
#endif
