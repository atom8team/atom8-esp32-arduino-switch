#include "Atom8_MCU_Switch.h"
#include "Arduino.h"
#include "ESP32Ticker.h"
#include "Atom8_Cloud.h"
#include "Atom8_Storage.h"
#include "Atom8_Utils.h"
#include "Atom8_Constants.h"
#include "Atom8_Scene.h"
#include "Atom8_Wifi.h"
#include "Atom8_Events_Outgoing.h"
#include "Atom8_Aggregate.h"
#include "Atom8_Logging.h"

#define DEBOUNCE_TIME_IN_MILLIS 100 //100ms is the pulse period

#define DEBOUNCE_TIME_IN_MICROS DEBOUNCE_TIME_IN_MILLIS * 1000

typedef void(*fptr) ();

const byte ENABLE_PIN = 13;

const byte OUTPUT_SWITCH_PIN[] = {4, 16, 17, 5, 18, 19, 21, 22};

double OUTPUT_SWITCH_PIN_STATE[] =  { 0, 0, 0, 0 };

double INPUT_SWITCH_PIN_STATE[4] = { 0, 0, 0, 0 };

const int NO_OUTPUTS = sizeof(OUTPUT_SWITCH_PIN_STATE);

volatile unsigned long last_micros[4] = { 0, 0, 0, 0 };

volatile unsigned long last_count_ac[4] = { 0, 0, 0, 0 };

volatile unsigned long current_count_ac[4] = { 0, 0, 0, 0 };

volatile unsigned long last_micros_input_timer = 0;

bool stateChanged = true;

double val = 0;

uint acRead(uint);
void toggle(uint);
void recordACInterrupt0();
void recordACInterrupt1();
void recordACInterrupt2();
void recordACInterrupt3();
void changeIOISR();

const fptr INTERRUPT_CALLBACK[] = {
	recordACInterrupt0,
	recordACInterrupt1,
	recordACInterrupt2,
	recordACInterrupt3
};

void toggle(uint pinIndex) {
		int val = OUTPUT_SWITCH_PIN_STATE[pinIndex];
		val = 1 - val;
		atom8MCUSwitch.putSwitch(pinIndex, val);
		atom8EventsOutgoing.putInstanceEvent(pinIndex, val, "physical");
}

void debounceToggle(uint pinIndex) {
	// If the clock hasn't wrapped around then micros() will always be greater then last_micros of that pin index
	// If the clock has wrapped around then micros() will be less than last_micros
	if (micros() - last_micros[pinIndex] < DEBOUNCE_TIME_IN_MICROS )  return;
	toggle(pinIndex);
	last_micros[pinIndex] = micros();
}

void IRAM_ATTR recordACInterrupt0() {
	current_count_ac[0] += 1;
}

void IRAM_ATTR recordACInterrupt1() {
	current_count_ac[1] += 1;
}

void IRAM_ATTR recordACInterrupt2() {
	current_count_ac[2] += 1;
}

void IRAM_ATTR recordACInterrupt3() {
	current_count_ac[3] += 1;
}

uint8_t getOutputSwitchPinState(int pin) {
	String key = "instance_val_"+String(pin);
	String doubleValStr = atom8Storage.getKeyValue().get(key);
	if(doubleValStr == "") {
			return LOW;
	} else {
			return doubleValStr.toFloat();
	}
}

void setupOutputs() {
	for (int i = 0; i < sizeof(OUTPUT_SWITCH_PIN); i++) {
		pinMode(OUTPUT_SWITCH_PIN[i], OUTPUT);
		digitalWrite(OUTPUT_SWITCH_PIN[i], LOW);
		OUTPUT_SWITCH_PIN_STATE[i] = getOutputSwitchPinState(i);
	}
}

void setupInputs() {
	for (int i = 0; i < sizeof(atom8MCUSwitch.INPUT_SWITCH_PIN); i++) {
		int pin = atom8MCUSwitch.INPUT_SWITCH_PIN[i];
		pinMode(pin, INPUT_PULLUP);
		attachInterrupt(pin, INTERRUPT_CALLBACK[i], CHANGE);
	}
}

Ticker handleIOTimer;

void updateInputValues() {
	for(int i=0; i < 4; i++) {
		INPUT_SWITCH_PIN_STATE[i] = acRead(i);
	}
}

void Atom8MCUSwitch::setup() {
		setupOutputs();
		setupInputs();
		delay(1000);
		updateInputValues();
		handleIOTimer.attach_ms(DEBOUNCE_TIME_IN_MILLIS, changeIOISR);
}

void IRAM_ATTR handleInputs() {
	long micros_since_last_exec = micros() - last_micros_input_timer;
	if( micros_since_last_exec < 90000 || micros_since_last_exec > 110000 ) {
		last_micros_input_timer = micros();
		return;
	}

	last_micros_input_timer = micros();
	for(int pinIndex=0; pinIndex < 4; pinIndex++) {
		byte oldInputVal = INPUT_SWITCH_PIN_STATE[pinIndex];
		byte currentInputVal = acRead(pinIndex);

		if(oldInputVal != currentInputVal) {
			debounceToggle(pinIndex);
			INPUT_SWITCH_PIN_STATE[pinIndex] = currentInputVal;
			return;
		}
	}
}

void IRAM_ATTR changeIOISR() {
	handleInputs();
}


double Atom8MCUSwitch::getSwitch(byte pin) {
	return OUTPUT_SWITCH_PIN_STATE[pin];
}

double Atom8MCUSwitch::getAnalogValue() {
	return analogRead(A0);
}

static double roundVal(double val) {
	if(val > 0 && val <= 0.21) {
		val = 0.2;
	} else if(val > 0.2 && val <= 0.41) {
		val = 0.4;
	} else if(val > 0.4 && val <= 0.61) {
		val = 0.6;
	} else if(val > 0.6 && val <= 0.81 ) {
		val = 0.8;
	} else if (val > 0.81) {
		val = 1;
	} else {
		val = val;
	}
	return val;
}

void Atom8MCUSwitch::putSwitch(byte pin, double val) {
	val = roundVal(val);
	OUTPUT_SWITCH_PIN_STATE[pin] = val;
	for(int i = 0; i < 8; i++) {
		digitalWrite(OUTPUT_SWITCH_PIN[i], LOW);
	}
	if(val == 1){
		digitalWrite(OUTPUT_SWITCH_PIN[pin * 2], HIGH);
	} else {
		digitalWrite(OUTPUT_SWITCH_PIN[pin * 2 + 1], HIGH);
	}
	String key = "instance_val_"+String(pin);
	atom8Storage.getKeyValue().put(key.c_str(), String(val));
	stateChanged = true;
}


void Atom8MCUSwitch::loop() {

}

uint acRead(uint i) {
	if(current_count_ac[i] - last_count_ac[i] >= 16) {
		last_count_ac[i] = current_count_ac[i];
		return 1;
	} else {
		last_count_ac[i] = current_count_ac[i];
		return 0;
	}
}

Atom8MCUSwitch atom8MCUSwitch;
