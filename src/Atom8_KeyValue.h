#ifndef _ATOM8_KEYVALUE_h
#define _ATOM8_KEYVALUE_h

#include "ArduinoJson.h"
#include "map"
#include "FS.h"

class Atom8KeyValueEntry {
  public:
  String key;
  String value;
};

class Atom8KeyValue {
public:
  String storageName;
  Atom8KeyValue(String storageName);

  bool put(String key, String value);
  String get(String key);
  bool remove(String key);
  bool clear();
};

#endif
