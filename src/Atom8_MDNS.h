#ifndef _ATOM8_OTA_h
#define _ATOM8_OTA_h
#endif

#if defined(ARDUINO) && ARDUINO >= 100
	#include "Arduino.h"
#else
	#include "WProgram.h"
#endif

class Atom8_MDNS
{
private:
	void refreshIpAddresses();
public:
  void loop();

	String getIp(String deviceName);
};

extern Atom8_MDNS atom8MDNS;
