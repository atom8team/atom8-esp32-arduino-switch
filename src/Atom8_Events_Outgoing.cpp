#include "Atom8_Events_Outgoing.h"
#include "Arduino.h"
#include "list"
#include "Atom8_Cloud.h"
#include "Atom8_Wifi.h"
#include "Atom8_Constants.h"
#include "Atom8_Aggregate.h"
#include "StreamString.h"
#include "Atom8_Crash_Handler.h"

#define CRASH_EVENT_DEBOUNCE ONE_SEC_MILLIS * 20

std::list<String> eventsList;

void Atom8EventsOutgoing::push(String eventData) {
  eventsList.push_back(eventData);
}

void Atom8EventsOutgoing::publishPendingEventsToCloudDebounce() {
  if(!eventsList.empty()) {
    String eventsData = eventsList.front();
    atom8Cloud.publish(eventsData);
    eventsList.pop_front();
    delay(0);
  }
}

void Atom8EventsOutgoing::publishModuleRestartEventToCloud() {
  DynamicJsonBuffer jsonBuffer;

  JsonObject& details = jsonBuffer.createObject();
  details["deviceName"] = atom8WifiManager.getSoftAPSSID();
  details["ssid"] = atom8WifiManager.getSSID();
  // details["resetReason"] = ESP.getResetReason();
  // details["resetInfo"] = ESP.getResetInfo();

  JsonObject& eventJson = jsonBuffer.createObject();
  eventJson["type"] = "modulerestart";
  eventJson["details"] = details;

  String eventJsonStr;
  eventJson.printTo(eventJsonStr);
  push(eventJsonStr);
}

void Atom8EventsOutgoing::publishDeviceUpdateEventToCloud() {
  String responseString = atom8Aggregate.getInfo("high");
	DynamicJsonBuffer jsonBuffer;
	JsonObject& responseJson = jsonBuffer.parseObject(responseString);

  JsonObject& deviceUpdateJson = jsonBuffer.createObject();
	deviceUpdateJson["type"] = "deviceupdate";
	deviceUpdateJson["details"] = responseJson;

  String deviceUpdateStr;
	deviceUpdateJson.printTo(deviceUpdateStr);
	push(deviceUpdateStr);
}

void Atom8EventsOutgoing::publishModuleRestartEventToCloudDebounce() {
  if(sendModuleRestartEvent) {
    publishModuleRestartEventToCloud();
    delay(0);
    publishDeviceUpdateEventToCloud();
    sendModuleRestartEvent = false;
  }
}

void Atom8EventsOutgoing::publishPingEventToCloud() {
  String responseString = atom8Aggregate.getInfo("low");
	DynamicJsonBuffer jsonBuffer;
	JsonObject& responseJson = jsonBuffer.parseObject(responseString);

  JsonObject& deviceUpdateJson = jsonBuffer.createObject();
	deviceUpdateJson["type"] = "ping";
	deviceUpdateJson["details"] = responseJson;

  String deviceUpdateStr;
	deviceUpdateJson.printTo(deviceUpdateStr);
	push(deviceUpdateStr);
}


void Atom8EventsOutgoing::publishPingEventToCloudDebounce() {
  if(millis() < lastPingTimestamp
      || (millis() < ONE_MIN_MILLIS && firstPingSent == false)
      || millis() - lastPingTimestamp > ONE_MIN_MILLIS) {
    publishPingEventToCloud();
		lastPingTimestamp = millis();
    firstPingSent = true;
	}
}

void Atom8EventsOutgoing::publishCrashEventsToCloud() {
  DynamicJsonBuffer jsonBuffer;

  JsonObject& crashDetails = jsonBuffer.createObject();
  crashDetails["stackTrace"] = atom8CrashHandler.getCrashStr();
  crashDetails["id"] = atom8WifiManager.getSoftAPSSID();
  crashDetails["ssid"] = atom8WifiManager.getSSID();
  crashDetails["bssid"] = atom8WifiManager.getBSSID();
  // crashDetails["resetReason"] = ESP.getResetReason();
  // crashDetails["resetInfo"] = ESP.getResetInfo();
  #ifdef ATOM8_SWITCH_MODULE
  crashDetails["versionCode"] = ATOM8_FIRMWARE_SWITCH_VERSION_CODE;
  crashDetails["versionName"] = ATOM8_FIRMWARE_SWITCH_VERSION_NAME;
  #endif

  JsonObject& responseJson = jsonBuffer.createObject();
  responseJson["type"] = "crash";
  responseJson["details"] = crashDetails;

  String crashDetailsStr;
  responseJson.printTo(crashDetailsStr);
  push(crashDetailsStr);
}

void Atom8EventsOutgoing::publishCrashEventsToCloudDebounce() {
    if((millis() - syncRequestTimestamp > CRASH_EVENT_DEBOUNCE))  {
      if(atom8CrashHandler.hasCrash()) {
        publishCrashEventsToCloud();
        syncRequestTimestamp = millis();
        atom8CrashHandler.clear();
      }
    }
}

void Atom8EventsOutgoing::publishPrimingEventToCloud() {
  DynamicJsonBuffer jsonBuffer;
  JsonObject& primingDetails = jsonBuffer.createObject();
  primingDetails["nodeId"] = atom8WifiManager.getSoftAPSSID();
  // primingDetails["resetReason"] = ESP.getResetReason();
  // primingDetails["resetInfo"] = ESP.getResetInfo();

  JsonObject& primingRequestJson = jsonBuffer.createObject();
  primingRequestJson["type"] = "priming";
  primingRequestJson["details"] = primingDetails;

  String primingRequestJsonStr;
  primingRequestJson.printTo(primingRequestJsonStr);

  push(primingRequestJsonStr);
}

void Atom8EventsOutgoing::publishPrimingEventToCloudDebounce() {
  if(millis() - lastPrimePublished > 1000) {
    publishPrimingEventToCloud();
    lastPrimePublished = millis();
  }
}

void Atom8EventsOutgoing::loop() {
  delay(0);
  publishPendingEventsToCloudDebounce();
  if(atom8Cloud.isReady()) {
    delay(0);
    publishModuleRestartEventToCloudDebounce();
    delay(0);
    publishPingEventToCloudDebounce();
    delay(0);
    publishCrashEventsToCloudDebounce();
  } else {
    delay(0);
    publishPrimingEventToCloudDebounce();
  }
}

void Atom8EventsOutgoing::putInstanceEvent(int pin, double val, String actionType) {
  DynamicJsonBuffer jsonBuffer;

  JsonObject& details = jsonBuffer.createObject();
  details["access"] = actionType;
  details["deviceName"] = atom8WifiManager.getSoftAPSSID();
  details["ssid"] = atom8WifiManager.getSSID();
  details["instance"] = pin;
  details["value"] = val;

  JsonObject& eventJson = jsonBuffer.createObject();
  eventJson["type"] = "putInstance";
  eventJson["details"] = details;

  String eventJsonStr;
  eventJson.printTo(eventJsonStr);
  push(eventJsonStr);
}

void Atom8EventsOutgoing::automationExecutedEvent(String sceneId, String sceneName) {
  DynamicJsonBuffer jsonBuffer;

  JsonObject& details = jsonBuffer.createObject();
  details["deviceName"] = atom8WifiManager.getSoftAPSSID();
  details["ssid"] = atom8WifiManager.getSSID();
  details["sceneId"] = sceneId;
  details["sceneName"] = sceneName;

  JsonObject& eventJson = jsonBuffer.createObject();
  eventJson["type"] = "automation";
  eventJson["details"] = details;

  String eventJsonStr;
  eventJson.printTo(eventJsonStr);
  push(eventJsonStr);
}

Atom8EventsOutgoing atom8EventsOutgoing;
