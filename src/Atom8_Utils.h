#include "WiFi.h"
#include "list"

class Atom8Utils {
private:
	const char* getStoredJson();
public:
	String ipAddressToString(IPAddress ipAddress);
	bool isEmpty(const char* input);
	bool isEmpty(String input);
	bool isNotEmpty(String input);
	String toString(uint64_t number);
	String charAndLenToString(char* input, int inputLen);
};

extern Atom8Utils atom8Utils;
