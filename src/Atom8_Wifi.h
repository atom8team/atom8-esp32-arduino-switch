#include "WString.h"
#include "WiFi.h"

#ifndef _ATOM8_WIFI_h
#define _ATOM8_WIFI_h
#include "ArduinoJson.h"


class Atom8WifiManager {
public:
	bool enqueueForConnectToAP(String ssid, String password);
	bool connectToAP(String ssid, String password);
	void setup();
	void loop();
	const char* getSoftAPSSID();
	IPAddress getIp();
  bool isConnected();
  bool isNotConnected();
	int waitForWifiConnection();
	int getStatus();
	int getEnqueueStatus();
	int rssi();
	String getSSID();
	String getBSSID();
};

typedef enum {
		WL_EMPTY_QUEUE 								= 7,
		WL_CONNECTION_IN_QUEUE        = 8,
    WL_CONNECTING        					= 9
} atom8_wl_status_t;

extern Atom8WifiManager atom8WifiManager;

#endif
