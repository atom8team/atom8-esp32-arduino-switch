#include "Atom8_Wifi.h"
#include "WiFi.h"
#include "Atom8_Utils.h"
#include "Atom8_Storage.h"
#include "ESP32Ticker.h"
#include "Atom8_Constants.h"
#include "Atom8_Logging.h"

String ssidAPStr = "Atom8-" +atom8Utils.toString(ESP.getEfuseMac()) ;
const char *ssidAP = ssidAPStr.c_str();
const char *ssidPasswd = "el159fu852mu";

String ssidEnqueue;
String passwordEnqueue;
bool isEnqueueAvailable = false;
int enqueueStatus = WL_EMPTY_QUEUE;


bool Atom8WifiManager::enqueueForConnectToAP(String ssid, String password) {
  ssidEnqueue = ssid;
	passwordEnqueue = password;
	isEnqueueAvailable = true;
  enqueueStatus = WL_CONNECTION_IN_QUEUE;
  atom8Logging.info("Enqueue done");
	return true;
}

bool Atom8WifiManager::connectToAP(String ssid, String password) {
  WiFi.begin(ssid.c_str(), password.c_str());
  int status = waitForWifiConnection();
  if(status == WL_CONNECTED) {
    WiFi.mode(WIFI_STA);
    atom8Storage.getKeyValue().put("ssid", ssid);
    atom8Storage.getKeyValue().put("password", password);
    return true;
  } else {
  	WiFi.mode(WIFI_AP);
    WiFi.softAP(ssidAP);
    return false;
  }
}

const char* Atom8WifiManager::getSoftAPSSID() {
	return ssidAP;
}

int Atom8WifiManager::getStatus() {
	return WiFi.status();
}

int Atom8WifiManager::getEnqueueStatus() {
	return enqueueStatus;
}

bool Atom8WifiManager::isConnected() {
  return getStatus() == WL_CONNECTED;
}

bool Atom8WifiManager::isNotConnected() {
  return !isConnected();
}

IPAddress Atom8WifiManager::getIp() {
	if (isConnected()) return WiFi.localIP();
}

int Atom8WifiManager::rssi() {
	return WiFi.RSSI();
}

String Atom8WifiManager::getSSID() {
  return WiFi.SSID();
}

String Atom8WifiManager::getBSSID() {
  return WiFi.BSSIDstr();
}

  // https://github.com/esp8266/Arduino/blob/0c897c37a6eab3eab34147219617945a32a9b155/libraries/ESP8266WiFi/src/include/wl_definitions.h#L50
int Atom8WifiManager::waitForWifiConnection() {
	while(getStatus() != WL_NO_SSID_AVAIL && getStatus() != WL_CONNECT_FAILED && getStatus() != WL_CONNECTED) {
    atom8Logging.info("Connecting Status = " + String(getStatus()));
		delay(1000);
	}
  atom8Logging.info("Connecting Status = " + String(getStatus()));
	return getStatus();
}


void Atom8WifiManager::setup() {
	/* Set these to your desired credentials. */
  String ssid = atom8Storage.getKeyValue().get("ssid");
  String password = atom8Storage.getKeyValue().get("password");
  if(atom8Utils.isNotEmpty(ssid) && atom8Utils.isNotEmpty(password)) {
    atom8Logging.info("Connecting to stored credentials");
    atom8Logging.info(ssid);
    WiFi.disconnect(true, true);
    connectToAP(ssid, password);
  } else {
    atom8Logging.info("Stored credentials not found. Waiting for configuration");
    WiFi.mode(WIFI_AP);
  }
}

void Atom8WifiManager::loop() {
	if(isEnqueueAvailable) {
    atom8Logging.info("Enqueue available connecting to new ssid "+ssidEnqueue);
    connectToAP(ssidEnqueue, passwordEnqueue);
    enqueueStatus = WL_CONNECTING;
    isEnqueueAvailable = false;
	}
}

Atom8WifiManager atom8WifiManager;
