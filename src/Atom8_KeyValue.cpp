#include "Atom8_KeyValue.h"
#include "Atom8_Utils.h"
#include "map"
#include "Atom8_Constants.h"
#include "Preferences.h"


Preferences preferences;



Atom8KeyValue::Atom8KeyValue(String storageName) {
	this->storageName = storageName;
}

/*************************************************************************************
 * Private Section
 */



/*************************************************************************************
* End of Private Section
*/


/*************************************************************************************
* Public Section
*/

bool Atom8KeyValue::put(String key, String value) {
	preferences.begin(this->storageName.c_str(), false);
	preferences.putString(key.c_str(), value.c_str());
	preferences.end();
}

String Atom8KeyValue::get(String key) {
	preferences.begin(this->storageName.c_str(), false);
	String value = preferences.getString(key.c_str());
	preferences.end();
	return value;
}

bool Atom8KeyValue::remove(String key) {
	preferences.begin(this->storageName.c_str(), false);
	preferences.remove(key.c_str());
	preferences.end();
}

bool Atom8KeyValue::clear() {
	preferences.begin(this->storageName.c_str(), false);
	preferences.clear();
	preferences.end();
}
