#include "ArduinoJson.h"
#include "map"
#include "Atom8_KeyValue.h"

class Atom8Storage {
private:
  Atom8KeyValue keyValue = Atom8KeyValue("keyValue");
public:
  void loop();
  Atom8KeyValue& getKeyValue();
};

extern Atom8Storage atom8Storage;
