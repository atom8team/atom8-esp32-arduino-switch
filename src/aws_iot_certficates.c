/*
 * Copyright 2010-2015 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 * Additions Copyright 2016 Espressif Systems (Shanghai) PTE LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 *
 *  http://aws.amazon.com/apache2.0
 *
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

/**
 * @file aws_iot_certifcates.c
 * @brief File to store the AWS certificates in the form of arrays
 */

#ifdef __cplusplus
extern "C" {
#endif

const char aws_root_ca_pem[] = {"-----BEGIN CERTIFICATE-----\n\
MIIE0zCCA7ugAwIBAgIQGNrRniZ96LtKIVjNzGs7SjANBgkqhkiG9w0BAQUFADCB\n\
yjELMAkGA1UEBhMCVVMxFzAVBgNVBAoTDlZlcmlTaWduLCBJbmMuMR8wHQYDVQQL\n\
ExZWZXJpU2lnbiBUcnVzdCBOZXR3b3JrMTowOAYDVQQLEzEoYykgMjAwNiBWZXJp\n\
U2lnbiwgSW5jLiAtIEZvciBhdXRob3JpemVkIHVzZSBvbmx5MUUwQwYDVQQDEzxW\n\
ZXJpU2lnbiBDbGFzcyAzIFB1YmxpYyBQcmltYXJ5IENlcnRpZmljYXRpb24gQXV0\n\
aG9yaXR5IC0gRzUwHhcNMDYxMTA4MDAwMDAwWhcNMzYwNzE2MjM1OTU5WjCByjEL\n\
MAkGA1UEBhMCVVMxFzAVBgNVBAoTDlZlcmlTaWduLCBJbmMuMR8wHQYDVQQLExZW\n\
ZXJpU2lnbiBUcnVzdCBOZXR3b3JrMTowOAYDVQQLEzEoYykgMjAwNiBWZXJpU2ln\n\
biwgSW5jLiAtIEZvciBhdXRob3JpemVkIHVzZSBvbmx5MUUwQwYDVQQDEzxWZXJp\n\
U2lnbiBDbGFzcyAzIFB1YmxpYyBQcmltYXJ5IENlcnRpZmljYXRpb24gQXV0aG9y\n\
aXR5IC0gRzUwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCvJAgIKXo1\n\
nmAMqudLO07cfLw8RRy7K+D+KQL5VwijZIUVJ/XxrcgxiV0i6CqqpkKzj/i5Vbex\n\
t0uz/o9+B1fs70PbZmIVYc9gDaTY3vjgw2IIPVQT60nKWVSFJuUrjxuf6/WhkcIz\n\
SdhDY2pSS9KP6HBRTdGJaXvHcPaz3BJ023tdS1bTlr8Vd6Gw9KIl8q8ckmcY5fQG\n\
BO+QueQA5N06tRn/Arr0PO7gi+s3i+z016zy9vA9r911kTMZHRxAy3QkGSGT2RT+\n\
rCpSx4/VBEnkjWNHiDxpg8v+R70rfk/Fla4OndTRQ8Bnc+MUCH7lP59zuDMKz10/\n\
NIeWiu5T6CUVAgMBAAGjgbIwga8wDwYDVR0TAQH/BAUwAwEB/zAOBgNVHQ8BAf8E\n\
BAMCAQYwbQYIKwYBBQUHAQwEYTBfoV2gWzBZMFcwVRYJaW1hZ2UvZ2lmMCEwHzAH\n\
BgUrDgMCGgQUj+XTGoasjY5rw8+AatRIGCx7GS4wJRYjaHR0cDovL2xvZ28udmVy\n\
aXNpZ24uY29tL3ZzbG9nby5naWYwHQYDVR0OBBYEFH/TZafC3ey78DAJ80M5+gKv\n\
MzEzMA0GCSqGSIb3DQEBBQUAA4IBAQCTJEowX2LP2BqYLz3q3JktvXf2pXkiOOzE\n\
p6B4Eq1iDkVwZMXnl2YtmAl+X6/WzChl8gGqCBpH3vn5fJJaCGkgDdk+bW48DW7Y\n\
5gaRQBi5+MHt39tBquCWIMnNZBU4gcmU7qKEKQsTb47bDN0lAtukixlE0kF6BWlK\n\
WE9gyn6CagsCqiUXObXbf+eEZSqVir2G3l6BFoMtEMze/aiCKm0oHw0LxOXnGiYZ\n\
4fQRbxC1lfznQgUy286dUV4otp6F01vvpX1FQHKOtw5rDgb7MzVIcbidJ4vEZV8N\n\
hnacRHr2lVz2XTIIM6RUthg/aFzyQkqFOFSDX9HoLPKsEdao7WNq\n\
-----END CERTIFICATE-----\n"};

const char certificate_pem_crt[] = {"-----BEGIN CERTIFICATE-----\n\
MIIDWjCCAkKgAwIBAgIVAJGOoZbJAvFm/bv137MsQ80dQrRaMA0GCSqGSIb3DQEB\n\
CwUAME0xSzBJBgNVBAsMQkFtYXpvbiBXZWIgU2VydmljZXMgTz1BbWF6b24uY29t\n\
IEluYy4gTD1TZWF0dGxlIFNUPVdhc2hpbmd0b24gQz1VUzAeFw0xNzEyMTEwNTMw\n\
MTBaFw00OTEyMzEyMzU5NTlaMB4xHDAaBgNVBAMME0FXUyBJb1QgQ2VydGlmaWNh\n\
dGUwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDO1hbiuiWTaYpkuxhr\n\
qO4T8jv/f39zT0KzP90A28kKOsWNFHP10/GudEmc5ckapMmulqlcV6R7lxxewe2Q\n\
pJ39VrH8REKtaYGjB34hYkH7zXWuoMfw9K1Xfc9cEoZzrJCTZmROqy5dXRlZOwsU\n\
vqA3b5HH66uPtEuSOf5JdgcnthEYJrHH3AD/KLlX6KjzkEWr6KWaeIeRM630IO0P\n\
tZZlsgIPjZez11InrCVnPsuq9agLHXc5Mc9zBFGtCO1qsfDH6NxJy928ghz4kkBK\n\
CRt9jWBXsu+Gun+PnQuP+s3jjM31CZX+tWAMmrKlfw1JkUuDR2uFtvJX6NMuYmAz\n\
l7DlAgMBAAGjYDBeMB8GA1UdIwQYMBaAFF10MDLP3tNFGc8/VR4pok5xrxVNMB0G\n\
A1UdDgQWBBTVIvif2tZr1cKRheb4pwAJfbbCNTAMBgNVHRMBAf8EAjAAMA4GA1Ud\n\
DwEB/wQEAwIHgDANBgkqhkiG9w0BAQsFAAOCAQEADCoJmXY1++jQvUfnzqebZWcc\n\
NHc5JzU+UZW+Dp45vkBuYG7wQJe0Fjdpw7j3spnq9si6PBL0kNZfHKT1tkgyL68Q\n\
2GJPQkGEPcxtiDAJv4TdJckaqnYVRUOPUG+EAW/zRz7iKBVe6VpTDnNcdRVEVuXM\n\
CY5kBGRLsJslSnzy/cxwhe4fKxpqku0/CpJHwowhNsPfFIbqFpzqX/Q81JIJMg4T\n\
TuZzJ0elTknL6PkIgKt4IdMCZr8E0uPjGdzzLAOtJelPgUoEPA4+qej3yHc/pFVX\n\
frtaI91VzTs/yDik1iRKPRc/RkbwnRc3NiHxn107pTp0LJVOc93oaZSGcrgOLQ==\n\
-----END CERTIFICATE-----\n"};

const char private_pem_key[] = {"-----BEGIN RSA PRIVATE KEY-----\n\
MIIEpAIBAAKCAQEAztYW4rolk2mKZLsYa6juE/I7/39/c09Csz/dANvJCjrFjRRz\n\
9dPxrnRJnOXJGqTJrpapXFeke5ccXsHtkKSd/Vax/ERCrWmBowd+IWJB+811rqDH\n\
8PStV33PXBKGc6yQk2ZkTqsuXV0ZWTsLFL6gN2+Rx+urj7RLkjn+SXYHJ7YRGCax\n\
x9wA/yi5V+io85BFq+ilmniHkTOt9CDtD7WWZbICD42Xs9dSJ6wlZz7LqvWoCx13\n\
OTHPcwRRrQjtarHwx+jcScvdvIIc+JJASgkbfY1gV7Lvhrp/j50Lj/rN44zN9QmV\n\
/rVgDJqypX8NSZFLg0drhbbyV+jTLmJgM5ew5QIDAQABAoIBAQDA/aXw+7tf5pRJ\n\
FQHeQouSdO1iorZrA0O7ORWfXXQiZWA7GSBx+caB/uNI0GdqwuOkUdPI48YHw6Gn\n\
EM/3rIH2BlSgi5ijcdSSJ8WapLHon3b07/Rfcg3jmvJGeMqXWCa2Qk3J0ZO7bcjf\n\
J2ZZftzDnQirP8yek54H3LDWHpjihSOMSXh4z6ToLLAIYn4sAzq7bWdsCIiBeBpa\n\
yhBxWTEeLCTaxfWz7XwYNK6URWs6/LarD7lIl1P4nvHIPjQ4N6sHbOBuYVzotqCV\n\
zwtl/qoNlg9D4/2PFwUFskw1C1izkLsYCScRk3InXEqd0m4LcRJleJpm8GxBsR5e\n\
2WqX3HjpAoGBAO7DenSzKxdefLD/YaXc+s7j4XRT/Jb83AKZlO1zUOVmxzw0O3Fo\n\
SO1wd4sBWGG1/hcOQmi9PFwT9io3y+ILk1Rb9A4F9BpcdUtu9tYfZYTzVYUH6lKE\n\
5z/f1G+JIQrYDUgVDFJl9DypNxrVHbpZrFocpBWRMy++f4ltQq4yPa2/AoGBAN3E\n\
kl0BXbRRhEkp+N0hx+YhEqSa+YwL7evDJ9IMXXJpTj8XgGY0on8d1jACMjUBWcos\n\
3S6NCW8vYoJKQrpHWXmvwGz+B+Swwgj9Nm7cADvEzXWSxQPp0VsliK7imz4sGrpP\n\
mTIJKGj/LCgNexrDn1LIh3z5fBXqw7kYw2uibJJbAoGAI88TfY8lhlK4dr9GgN68\n\
iuzock7qUibIRGeOS3KBDna0qNXYmHCgh2PXufRQsg7edczArv66n8CqqbhGjrdX\n\
uE42Myg3bQCy4sZYpNJ+okrwZ4xBW3DtUcobDUNOvlaa31WwzGbYE4T5a7/MvpcW\n\
X0luXoGxYJa8f/rwBjoov/cCgYEAoh0kcWCiQ0Kj1WgdSGl9W7kpi7LOYZYG4qXS\n\
4jWvygc4794aOpyXfR0uigHQ+fZ20NhjB7iKAQE+ncfzZ4Do44+atxIsavzbmv6f\n\
YALrGMYQ+7XC182BAYQ6mr2Ehrpu/2tYrb8MD1UZzl2sdDA0s6trf+xQzSTE1vZH\n\
RNODUqkCgYAKbzBkXzbQHbIG87mO8JppaEzw9lctB/uh9oygFI7o8Fr6vUgQ2Vmi\n\
pH/gESEU5NdRT9D9Z5M73O6dA77if+iCH0qHgNlxuoonE76AHbggRRoqiYspYcna\n\
C+JiqVL4tdI0K/AfPm0N8Y2991QYr20bI2rW27NfL8Vb0LIiVYjYFw==\n\
-----END RSA PRIVATE KEY-----\n"};


#ifdef __cplusplus
}
#endif
