/***************************************************************************************************
                                    ExploreEmbedded Copyright Notice
****************************************************************************************************
 * File:   AWS_IOT.cpp
 * Version: 1.0
 * Author: ExploreEmbedded
 * Website: http://www.exploreembedded.com/wiki
 * Description: ESP32  Arduino library for AWS IOT.

This code has been developed and tested on ExploreEmbedded boards.
We strongly believe that the library works on any of development boards for respective controllers.
Check this link http://www.exploreembedded.com/wiki for awesome tutorials on 8051,PIC,AVR,ARM,Robotics,RTOS,IOT.
ExploreEmbedded invests substantial time and effort developing open source HW and SW tools, to support consider buying the ExploreEmbedded boards.

The ExploreEmbedded libraries and examples are licensed under the terms of the new-bsd license(two-clause bsd license).
See also: http://www.opensource.org/licenses/bsd-license.php

EXPLOREEMBEDDED DISCLAIMS ANY KIND OF HARDWARE FAILURE RESULTING OUT OF USAGE OF LIBRARIES, DIRECTLY OR
INDIRECTLY. FILES MAY BE SUBJECT TO CHANGE WITHOUT PRIOR NOTICE. THE REVISION HISTORY CONTAINS THE INFORMATION
RELATED TO UPDATES.


Permission to use, copy, modify, and distribute this software and its documentation for any purpose
and without fee is hereby granted, provided that this copyright notices appear in all copies
and that both those copyright notices and this permission notice appear in supporting documentation.
**************************************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <unistd.h>
#include <limits.h>
#include <string.h>
#include "AWS_IOT.h"
#include "aws_iot_config.h"
#include "aws_iot_log.h"
#include "aws_iot_version.h"
#include "aws_iot_mqtt_client.h"
#include "aws_iot_mqtt_client_interface.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_system.h"
#include "esp_wifi.h"
#include "esp_event_loop.h"
#include "esp_log.h"
#include "esp_vfs_fat.h"
#include "driver/sdmmc_host.h"
#include "map"
#include "utility"      // std::pair
#include "Arduino.h"
#include "Atom8_Logging.h"
#include "Atom8_Utils.h"

#define TAG "AWS_IOT"
#define  MAX_CLIENT_ID_SIZE 50

AWS_IoT_Client iotClient;
char clientId[50];
char awsIotHostAddress[128];
std::map<String, pSubCallBackHandler_t> subscribeHandlers;


/* CA Root certificate, device ("Thing") certificate and device
 * ("Thing") key. "Embedded Certs" are stored in the file aws_iot_certificates.c as arrays
 */
extern const char aws_root_ca_pem[];
extern const char certificate_pem_crt[];
extern const char private_pem_key[];

void aws_iot_task(void *param);
IoT_Error_t connectInternal( char *hostAddress, char *clientID);

void iot_subscribe_callback_handler(AWS_IoT_Client *pClient, char *topicName, uint16_t topicNameLen,
        IoT_Publish_Message_Params *params, void *pData) {
    if(subscribeHandlers.size() == 0) return;
    String topicNameStr = atom8Utils.charAndLenToString(topicName, topicNameLen);
    String payloadStr = atom8Utils.charAndLenToString((char *)params->payload, params->payloadLen);
    subscribeHandlers[topicNameStr](topicNameStr, payloadStr);
}

int subscribeInternal(char *subTopic, pSubCallBackHandler_t pSubCallBackHandler) {
    ESP_LOGE(TAG, "Subscribing...");
    atom8Logging.info(String(subTopic));
    IoT_Error_t rc = aws_iot_mqtt_subscribe(&iotClient, subTopic, strlen(subTopic), QOS1, iot_subscribe_callback_handler, NULL);
    if(SUCCESS != rc) {
        ESP_LOGE(TAG, "Error subscribing : %d ", rc);
        return rc;
    }
    subscribeHandlers[subTopic] = pSubCallBackHandler;
    ESP_LOGE(TAG, "Subscribing... Successful");
    return rc;
}

void disconnectCallbackHandler(AWS_IoT_Client *pClient, void *data) {
    ESP_LOGW(TAG, "MQTT Disconnect");
    IoT_Error_t rc = FAILURE;

    if(NULL == pClient) return;

    if(aws_iot_is_autoreconnect_enabled(pClient)) {
        ESP_LOGE(TAG, "Auto Reconnect is enabled, Reconnecting attempt will start now");
    } else {
        ESP_LOGE(TAG, "Auto Reconnect not enabled. Starting manual reconnect...");
        rc = connectInternal(awsIotHostAddress, clientId);
        if(NETWORK_RECONNECTED == rc) {
            ESP_LOGE(TAG, "Manual Reconnect Successful");
            // show content:
        } else {
            ESP_LOGE(TAG, "Manual Reconnect Failed - %d", rc);
        }
    }
}

IoT_Error_t connectInternal( char *hostAddress, char *clientID){
  const size_t stack_size = 36*1024;
  strcpy(awsIotHostAddress,hostAddress);
  IoT_Error_t rc = FAILURE;
  IoT_Client_Init_Params mqttInitParams = iotClientInitParamsDefault;
  IoT_Client_Connect_Params connectParams = iotClientConnectParamsDefault;
  ESP_LOGI(TAG, "AWS IoT SDK Version %d.%d.%d-%s", VERSION_MAJOR, VERSION_MINOR, VERSION_PATCH, VERSION_TAG);

  mqttInitParams.enableAutoReconnect = false; // We enable this later below
  mqttInitParams.pHostURL = awsIotHostAddress;
  mqttInitParams.port = CONFIG_AWS_IOT_MQTT_PORT;

  mqttInitParams.pRootCALocation = (char *)aws_root_ca_pem;
  mqttInitParams.pDeviceCertLocation = (char *)certificate_pem_crt;
  mqttInitParams.pDevicePrivateKeyLocation = (char *)private_pem_key;

  mqttInitParams.mqttCommandTimeout_ms = 20000;
  mqttInitParams.tlsHandshakeTimeout_ms = 5000;
  mqttInitParams.isSSLHostnameVerify = true;
  mqttInitParams.disconnectHandler = disconnectCallbackHandler;
  mqttInitParams.disconnectHandlerData = NULL;

  rc = aws_iot_mqtt_init(&iotClient, &mqttInitParams);

  if(SUCCESS != rc) {
      ESP_LOGE(TAG, "aws_iot_mqtt_init returned error : %d ", rc);
      return rc; //abort();
  }

  connectParams.keepAliveIntervalInSec = 10;
  connectParams.isCleanSession = true;
  connectParams.MQTTVersion = MQTT_3_1_1;
  /* Client ID is set in the menuconfig of the example */
  connectParams.pClientID = clientID;
  connectParams.clientIDLen = (uint16_t) strlen(clientID);
  connectParams.isWillMsgPresent = false;

  ESP_LOGI(TAG, "Connecting to AWS...");

  rc = aws_iot_mqtt_connect(&iotClient, &connectParams);

  if(SUCCESS != rc) {
      ESP_LOGE(TAG, "Error(%d) connecting to %s:%d, \n\rTrying to reconnect", rc, mqttInitParams.pHostURL, mqttInitParams.port);
  }

  /*
   * Enable Auto Reconnect functionality. Minimum and Maximum time of Exponential backoff are set in aws_iot_config.h
   *  #AWS_IOT_MQTT_MIN_RECONNECT_WAIT_INTERVAL
   *  #AWS_IOT_MQTT_MAX_RECONNECT_WAIT_INTERVAL
   */
   rc = aws_iot_mqtt_autoreconnect_set_status(&iotClient, true);
   if(SUCCESS != rc) {
      ESP_LOGE(TAG, "Unable to set Auto Reconnect to true - %d", rc);
      abort();
   }

  if(rc == SUCCESS)
  xTaskCreatePinnedToCore(&aws_iot_task, "aws_iot_task", stack_size, NULL, 5, NULL, 1);
  return rc;
}

int AWS_IOT::connect(char *hostAddress, char *clientID) {
    return connectInternal(hostAddress, clientID);
}


int AWS_IOT::publish(char *pubtopic,char *pubPayLoad) {
    IoT_Publish_Message_Params paramsQOS0;
    paramsQOS0.qos = QOS0;
    paramsQOS0.payload = (void *) pubPayLoad;
    paramsQOS0.isRetained = 0;
    paramsQOS0.payloadLen = strlen(pubPayLoad);
    return aws_iot_mqtt_publish(&iotClient, pubtopic, strlen(pubtopic), &paramsQOS0);
}

int AWS_IOT::subscribe(char *subTopic, pSubCallBackHandler_t pSubCallBackHandler) {
    return subscribeInternal(subTopic, pSubCallBackHandler);
}

bool AWS_IOT::isConnected() {
  return aws_iot_mqtt_is_client_connected(&iotClient);
}


void aws_iot_task(void *param) {

IoT_Error_t rc = SUCCESS;

    while(1)
    {
        //Max time the yield function will wait for read messages
        rc = aws_iot_mqtt_yield(&iotClient, 200);

        if(NETWORK_ATTEMPTING_RECONNECT == rc)
        {
            // If the client is attempting to reconnect we will skip the rest of the loop.
            continue;
        }


        vTaskDelay(1000 / portTICK_RATE_MS);
    }
}
