#include "Atom8_Cloud.h"
#include <WiFi.h>
#include <AWS_IOT.h>
#include "Atom8_Wifi.h"
#include "Atom8_OTA.h"
#include "ArduinoJson.h"
#include "Atom8_MCU_Switch.h"
#include "Atom8_Time.h"
#include "Atom8_Events_Incoming.h"
#include <HTTPClient.h>
#include "Atom8_Logging.h"

/**
 * This is a custom MQTT server setup on EC2 and is bridged to AWS IOT as
 * AWS IOT relies on TLS1.2 and Esp8266 doesn't support TLS 1.2, Esp8266
 * runs on TLS 1.1 which connects to MQTT Server using username and password
 *
 * Setup Information:
 * https://aws.amazon.com/blogs/iot/how-to-bridge-mosquitto-mqtt-broker-to-aws-iot/
 */
char AWSIOT_SERVER_ENDPOINT[] = "a3dw9faflxgdj7.iot.us-east-1.amazonaws.com";

char MQTT_PUBLISH_TOPIC[] = "localgateway_to_awsiot";


bool ready = false;

AWS_IOT awsIOT;

bool isAWSIotConnected = false;
bool isAWSIotSubscribed = false;

void mySubCallBackHandler (String topicName, String payload) {
    atom8Logging.info(payload);
    // DynamicJsonBuffer jsonBuffer;
    // JsonObject& responseJson = jsonBuffer.parseObject(callbackJsonStr);
    //
    // String type = responseJson["type"];
    // if(type == "ota") {
    //   atom8EventsIncoming.handleOTAUpdateEvent(callbackJsonStr);
    // } else if(type == "deviceAction") {
    //   atom8EventsIncoming.handlePutSwitchEvent(callbackJsonStr);
    // } else if(type == "time") {
    //   atom8EventsIncoming.handleTimeUpdateEvent(callbackJsonStr);
    // } else if(type == "priming") {
    //   ready = true;
    // }

}


bool Atom8_Cloud::isReady() {
  return ready;
}

void Atom8_Cloud::publish(String publishStr) {
  // publishStr = publishStr+"\n";
  // uint8 payload[publishStr.length()+1];
  // publishStr.getBytes(payload, publishStr.length()+1);
  // MQTT::Publish newpub(MQTT_PUBLISH_TOPIC, payload, publishStr.length());
  // client.publish(newpub);
}

void Atom8_Cloud::setup() {
  ready = false;
}

void Atom8_Cloud::loop() {
  char clientId[50];
  strncpy(clientId, atom8WifiManager.getSoftAPSSID(), sizeof(clientId));
  if(isAWSIotConnected == false) {
      if(awsIOT.connect(AWSIOT_SERVER_ENDPOINT, clientId) == 0) {
        atom8Logging.info("Connected to AWS");
        isAWSIotConnected = true;
        isAWSIotSubscribed = false;
        delay(1000);
      }
  }
  if(awsIOT.isConnected() && isAWSIotSubscribed == false && awsIOT.subscribe("Device", mySubCallBackHandler) == 0) {
    // Serial.println(sizeof(mqttSubscribeTopicCharArr));
    atom8Logging.info("Subscribe Successfull");
    isAWSIotSubscribed = true;
  }
}

Atom8_Cloud atom8Cloud;
