#include "Arduino.h"
#include "ArduinoJson.h"
#include "Atom8_Time.h"
#include "Atom8_Storage.h"

long Atom8Time::getCurrentTime() {
  return this->currentTime;
}

void Atom8Time::setCurrentTime(long currentTime) {
  this->currentTime = currentTime;
}

long Atom8Time::getMillisSinceMidnight() {
  return this->millisSinceMidnight;
}

void Atom8Time::setMillisSinceMidnight(long millisSinceMidnight) {
  this->millisSinceMidnight = millisSinceMidnight;
}

Atom8Time atom8Time;
