#include "Atom8_Logging.h"
#include "Arduino.h"
// #include "BluetoothSerial.h"

// BluetoothSerial SerialBT;


void Atom8Logging::setup() {
  // SerialBT.begin("ESP32");
  // SerialBT.setDebugOutput(true);
  Serial.begin(115200);
  Serial.setDebugOutput(true);
}

void Atom8Logging::info(String message) {
	// SerialBT.println(message);
  Serial.println(message);
}

Atom8Logging atom8Logging;
