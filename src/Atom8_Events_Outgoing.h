#include "Arduino.h"

class Atom8EventsOutgoing {
private:
	void flush();
	void publishCrashEventsToCloud();
	void publishCrashEventsToCloudDebounce();
	void publishModuleRestartEventToCloud();
	void publishModuleRestartEventToCloudDebounce();
	void publishPendingEventsToCloud();
	void publishPendingEventsToCloudDebounce();
	void publishDeviceUpdateEventToCloud();
	void publishPingEventToCloud();
	void publishPingEventToCloudDebounce();
	void publishPrimingEventToCloud();
	void publishPrimingEventToCloudDebounce();
public:
	bool sendModuleRestartEvent = true;
	long lastPingTimestamp = 0;
	long syncRequestTimestamp = 0;
	long lastPrimePublished = 0;
	bool firstPingSent = false;
	void setup();
	void loop();
  void push(String eventData);
	void putInstanceEvent(int pin, double val, String actionType);
	void automationExecutedEvent(String sceneId, String sceneName);
};

extern Atom8EventsOutgoing atom8EventsOutgoing;
