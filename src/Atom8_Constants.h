#ifndef _ATOM8_CONSTANTS_h

  #define ATOM8_SWITCH_MODULE true
  #define ATOM8_FIRMWARE_SWITCH_VERSION_CODE 47
  #define ATOM8_FIRMWARE_SWITCH_VERSION_NAME "1.47"

  //#define ATOM8_SENSOR_MODULE true
  #define ATOM8_FIRMWARE_SENSOR_VERSION_CODE 1
  #define ATOM8_FIRMWARE_SENSOR_VERSION_NAME "1.1"



  #define ATOM8_DEBUG true;

  #include "Arduino.h"
  #include "map"
  #define _ATOM8_OTA_h
  const int ONE_MILLI_MICROS = 1000;
  const int ONE_SEC_MILLIS = 1000;
  const int ONE_MIN_MILLIS = ONE_SEC_MILLIS * 60;
  const int ONE_HOUR_MILLIS = ONE_MIN_MILLIS * 60;
  const int ONE_MIN_SECS = 60;
  const int ONE_HOUR_SECS = 60 * ONE_MIN_SECS;
  const int ONE_DAY_SECS = 24 * ONE_HOUR_SECS;
  const int FIVE_MIN_MILLIS = ONE_MIN_MILLIS * 5;

  #define HTTP_SUCCESS 200
  #define HTTP_FAILURE 500
  #define HTTP_ALREADY_EXISTS 409
  #define HTTP_BAD_INPUT 400

  #define GENERIC_DEVICE_TYPE_FAN_REGULATOR "FAN_REGULATOR"
  #define GENERIC_DEVICE_TYPE_MULTI_LEVEL_SWITCH "MULTI_LEVEL_SWITCH"
  #define GENERIC_DEVICE_TYPE_BINARY_SWITCH "BINARY_SWITCH"
  #define BINARY_SENSOR "BINARY_SENSOR"
  #define EMPTY_STRING ""


  typedef std::map<String, String>::iterator mapIterator;

  #define abs(x) ((x)>0?(x):-(x))

#endif
