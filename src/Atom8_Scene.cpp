
#include "Atom8_Scene.h"
#include "Atom8_Utils.h"
#include "list"
#include "Hash.h"
#include "Atom8_Storage.h"
#include "Atom8_Constants.h"
#include "Atom8_MDNS.h"
#include <HTTPClient.h>
#include "Atom8_Wifi.h"
#include "Atom8_MCU_Switch.h"
#include "Atom8_Time.h"
#include "Atom8_Events_Outgoing.h"
#include "Atom8_Logging.h"

std::list<String> sceneList;

boolean refreshAllScenes = true;

std::map<String, String> allScenes;

long lastMotionDetectedNotification = 0;

long lastMotionDetectedActionTaken = 0;

long lastMotionNotDetectedActionTaken = 0;

long lastSceneExecuted = 0;

void Atom8Scene::setup() {

  lastMotionNotDetectedActionTaken = millis();
  lastMotionDetectedActionTaken = millis();
}

void Atom8Scene::storeSceneInFS(String sceneJsonStr)  {
  DynamicJsonBuffer jsonBuffer;
  JsonObject& sceneJson = jsonBuffer.parseObject(sceneJsonStr);
  String sceneId = sceneJson["scene"]["sceneId"];
  sceneStorage.put(sceneId, sceneJsonStr);
}

void Atom8Scene::handleDeviceAction(String nodeId, String instanceId, double value) {
  String ip = atom8MDNS.getIp(nodeId);
  if(atom8Utils.isEmpty(nodeId) || atom8Utils.isEmpty(instanceId) || atom8Utils.isEmpty(ip)) return;
  HTTPClient http;
	String actionUrl = "http://"+ip+"/instance";
	atom8Logging.info("ActionUrl");
	atom8Logging.info(actionUrl);
	http.begin(actionUrl); //HTTPS
	DynamicJsonBuffer jsonBuffer;
	JsonObject& requestJson = jsonBuffer.createObject();
	requestJson["instance"] = instanceId;
	requestJson["value"] = value;
	String payload;
	requestJson.printTo(payload);
	int httpCode = http.sendRequest("PUT", (uint8_t *) payload.c_str(), payload.length());
  atom8Logging.info("Http Code"+String(httpCode));
	http.end();
}

void Atom8Scene::handleMotionActionForScenes(double value) {
  std::list<String> sceneToBeEvaluatedList = this->getAll();
  while(!sceneToBeEvaluatedList.empty()) {
    String sceneJsonStr = sceneToBeEvaluatedList.front();
    DynamicJsonBuffer jsonBuffer;
    JsonObject& sceneJson = jsonBuffer.parseObject(sceneJsonStr);
    String nodeId = sceneJson["deviceActions"][0]["nodeId"].asString();
    String instanceId = sceneJson["deviceActions"][0]["instanceId"].asString();
    handleDeviceAction(nodeId, instanceId, value);
    sceneToBeEvaluatedList.pop_front();
    delay(0);
  }
}

void Atom8Scene::handleMotionDetectedForScenes() {
  handleMotionActionForScenes(1);
}

void Atom8Scene::handleMotionNotDetectedForScenes() {
  handleMotionActionForScenes(0);
}

String getSceneExecutedKey(String sceneId) {
  return "scene_"+sceneId+"_executed";
}

void Atom8Scene::flushInMemoryStringToStorage() {
  if(!sceneList.empty()) {
    refreshAllScenes = true;
    String sceneJsonStr = sceneList.front();
    DynamicJsonBuffer jsonBuffer;
    JsonObject& sceneJson = jsonBuffer.parseObject(sceneJsonStr);
    String sceneId = sceneJson["scene"]["sceneId"];
    atom8Storage.getKeyValue().put(getSceneExecutedKey(sceneId), "false");
    this->storeSceneInFS(sceneJsonStr);
    sceneList.pop_front();
    delay(0);
  }
  // if(refreshAllScenes) {
  //   allScenes = sceneStorage.getAll();
  //   refreshAllScenes = false;
  // }
}



void Atom8Scene::evaluateAndExecuteScenesForSwitches() {
  std::list<String> evaluateSceneList = this->getAll();
  DynamicJsonBuffer jsonBuffer;
  while(!evaluateSceneList.empty()) {
    String sceneJsonStr = evaluateSceneList.front();
    JsonObject& sceneJson = jsonBuffer.parseObject(sceneJsonStr);
    String sceneId = sceneJson["scene"]["sceneId"];
    String sceneName = sceneJson["scene"]["name"];
    if(atom8Time.getMillisSinceMidnight() < ONE_MIN_MILLIS * 5) {
      atom8Storage.getKeyValue().put(getSceneExecutedKey(sceneId), "false");
    }
    String isExecuted = atom8Storage.getKeyValue().get(getSceneExecutedKey(sceneId));
    isExecuted.trim();
    if(strcmp(isExecuted.c_str(), "true") == 0) {
      evaluateSceneList.pop_front();
      delay(0);
      continue;
    }
    JsonArray& eventsArray = sceneJson["events"];
    boolean eventSatisfied = true;
    for(JsonArray::iterator it=eventsArray.begin(); it!=eventsArray.end(); ++it) {
        // *it contains the JsonVariant which can be casted as usuals
        JsonObject& value = *it;
        String eventType = value["eventType"];
        String eventDetailsJsonStr = value["eventDetails"];
        JsonObject& eventDetailsJson = jsonBuffer.parseObject(eventDetailsJsonStr);
        if(eventType == "TIME") {
          long startTime = eventDetailsJson["startTime"];
          long currentTime = atom8Time.getMillisSinceMidnight();
          if(currentTime < startTime || currentTime - startTime > ONE_HOUR_MILLIS ) {
             eventSatisfied = eventSatisfied && false;
          }
        } else if(eventType == "MOTION") {
          long deviceId = eventDetailsJson["id"];
          eventSatisfied = eventSatisfied && false;
        }
        delay(0);
    }
    if(eventSatisfied) {
      JsonArray& actionsArray = sceneJson["deviceActions"];
      bool deviceActionExecuted = false;
      for(JsonArray::iterator it=actionsArray.begin(); it!=actionsArray.end(); ++it) {
          // *it contains the JsonVariant which can be casted as usuals
          JsonObject& value = *it;
          const char* nodeId = value["nodeId"];
          byte instanceId = value["instanceId"];
          double val = value["value"];
          if(strcmp(nodeId, atom8WifiManager.getSoftAPSSID()) == 0) {
            atom8MCUSwitch.putSwitch(instanceId, val);
            deviceActionExecuted = true;
          }
          delay(0);
      }
      if(deviceActionExecuted) {
        atom8EventsOutgoing.automationExecutedEvent(sceneId, sceneName);
      }
      atom8Storage.getKeyValue().put(getSceneExecutedKey(sceneId), "true");
    }
    evaluateSceneList.pop_front();
    delay(0);
  }
}



void Atom8Scene::loop() {
  flushInMemoryStringToStorage();
  if(millis() - lastSceneExecuted < ONE_SEC_MILLIS * 30) return;
  evaluateAndExecuteScenesForSwitches();
  lastSceneExecuted = millis();
}

void Atom8Scene::setMotionDetected(long motionDetectedTime) {
  lastMotionDetectedNotification = motionDetectedTime;
}

int Atom8Scene::add(JsonObject& putSceneRequest) {
  JsonObject& sceneJson = putSceneRequest["sceneBO"];
  String sceneId = sceneJson["scene"]["sceneId"];
  String actionType = putSceneRequest["actionType"];

  // Validations
  if(atom8Utils.isEmpty(sceneId)) return HTTP_BAD_INPUT;
  if(actionType == "create" && this->exists(sceneId)) return HTTP_ALREADY_EXISTS;

  // Store Scene
  String sceneJsonStr;
  sceneJson.printTo(sceneJsonStr);
  sceneList.push_back(sceneJsonStr);
  return HTTP_SUCCESS;
}

bool Atom8Scene::remove(String sceneName) {
  sceneStorage.remove(sceneName);
  refreshAllScenes = true;
  return true;
}

bool Atom8Scene::exists(String sceneId) {
  std::list<String> allScenes = this->getAll();
  for (std::list<String>::iterator it=allScenes.begin(); it != allScenes.end(); ++it) {
    String sceneJsonStr = *it;
    DynamicJsonBuffer jsonBuffer;
    JsonObject& sceneJson = jsonBuffer.parseObject(sceneJsonStr);
    String eachSceneId = sceneJson["scene"]["sceneId"];
    sceneId.trim();
    eachSceneId.trim();
    if(eachSceneId == sceneId) return true;
  }
  return false;
}

std::list<String> Atom8Scene::getAll() {
  std::list<String> returnSceneList;
  for(mapIterator iterator = allScenes.begin(); iterator != allScenes.end(); iterator++) {
    String sceneJsonStr = iterator->second;
    DynamicJsonBuffer jsonBuffer;
    JsonObject& sceneJson = jsonBuffer.parseObject(sceneJsonStr);
    String eachSceneName = sceneJson["scene"]["name"];
    if(atom8Utils.isNotEmpty(eachSceneName)) {
      returnSceneList.push_back(sceneJsonStr);
      delay(0);
    }
	}
  return returnSceneList;
}

Atom8Scene atom8Scene;



// double currentTime = millis();
// if(lastMotionDetectedActionTaken < lastMotionDetectedNotification) {
//   handleMotionDetectedForScenes();
//   lastMotionDetectedActionTaken = millis();
// } else if (currentTime - lastMotionNotDetectedActionTaken > ONE_MIN_MILLIS * 30 && currentTime - lastMotionDetectedActionTaken > ONE_MIN_MILLIS * 30) {
//   handleMotionNotDetectedForScenes();
//   lastMotionNotDetectedActionTaken = millis();
// }
