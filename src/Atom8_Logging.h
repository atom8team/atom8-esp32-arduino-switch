#ifndef _ATOM8_LOGGING_h
#define _ATOM8_LOGGING_h
#include <Arduino.h>

class Atom8Logging {
public:
  void setup();
  void info(String);
};

extern Atom8Logging atom8Logging;

#endif
